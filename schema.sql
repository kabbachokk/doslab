create table statistics
(
    postId int,
    word text,
    freq int
);
ALTER TABLE statistics
 ADD CONSTRAINT post_word_uq
 UNIQUE (postId, word);