package worker

import (
	"bufio"
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"time"

	"github.com/lib/pq"
)

type Comment struct {
	PostID int    `json:"postId"`
	Body   string `json:"body"`
}

type Worker struct {
	db       *sql.DB
	interval time.Duration
	period   time.Duration
	client   http.Client
	request  *http.Request
}

func New(db *sql.DB, url string, interval time.Duration) *Worker {
	r, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	return &Worker{
		db:       db,
		interval: interval,
		period:   interval,
		client: http.Client{
			Timeout: time.Second * 3,
		},
		request: r,
	}
}

func (w *Worker) Start(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(w.period):
			break
		}

		start := time.Now()
		w.Action()
		end := time.Now()

		duration := end.Sub(start)
		w.period = w.interval - duration
	}
}

func (w *Worker) Action() {
	words := make(map[int]map[string]int)
	re := regexp.MustCompile("[a-zA-Z']+")

	res, err := w.client.Do(w.request)
	if err != nil {
		log.Fatal(err)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	r := bufio.NewReader(res.Body)
	d := json.NewDecoder(r)

	d.Token()
	for d.More() {
		comment := &Comment{}
		d.Decode(comment)

		matches := re.FindAllString(comment.Body, -1)

		if words[comment.PostID] == nil {
			words[comment.PostID] = make(map[string]int)
		}

		for _, match := range matches {
			words[comment.PostID][match]++
		}
	}
	d.Token()

	if err = updateStatistics(w.db, &words); err != nil {
		log.Println(err)
	}

}

func updateStatistics(db *sql.DB, stat *map[int]map[string]int) error {
	ids, freqs := []int{}, []int{}
	words := []string{}

	for postId, postWords := range *stat {
		for word, freq := range postWords {
			ids = append(ids, postId)
			words = append(words, word)
			freqs = append(freqs, freq)
		}
	}

	tx, err := db.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Exec(`
	CREATE TEMPORARY TABLE temp_statistics
	(LIKE statistics INCLUDING DEFAULTS)
	ON COMMIT DROP;
	`)
	if err != nil {
		tx.Rollback()
		return err
	}

	// 65535 не проблема, и так быстрее
	stmt := `
	INSERT INTO temp_statistics (postId, word, freq) (
	select * from unnest($1::int[], $2::text[], $3::int[])
	);`
	_, err = tx.Exec(stmt, pq.Array(ids), pq.Array(words), pq.Array(freqs))
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Exec(`
	INSERT INTO statistics
	SELECT * FROM temp_statistics
	ON CONFLICT ON CONSTRAINT post_word_uq DO UPDATE
	SET freq=EXCLUDED.freq;
	`)
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return err
}
