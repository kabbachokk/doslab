package handlers

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Statistics struct {
	PostID int    `json:"postId"`
	Word   string `json:"word"`
	Freq   int    `json:"count"`
}

type Handlers struct {
	db *sql.DB
}

func New(db *sql.DB) *Handlers {
	return &Handlers{db}
}

func (h *Handlers) HandleStatistics(c echo.Context) error {
	id := c.Param("id")
	rows, err := h.db.Query(`SELECT postId, word, freq FROM statistics WHERE postId = $1 ORDER BY freq DESC;`, id)
	if err != nil {
		log.Print(err)
		return c.NoContent(http.StatusInternalServerError)
	}
	defer rows.Close()

	res := []Statistics{}
	for rows.Next() {
		row := Statistics{}
		err := rows.Scan(&row.PostID, &row.Word, &row.Freq)
		if err != nil {
			log.Print(err)
			return c.NoContent(http.StatusInternalServerError)
		}
		res = append(res, row)
	}
	return c.JSON(http.StatusOK, res)
}
