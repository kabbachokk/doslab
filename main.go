package main

import (
	"context"
	"log"
	"net/http"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/kabbachokk/doslab/handlers"
	"gitlab.com/kabbachokk/doslab/postgres"
	"gitlab.com/kabbachokk/doslab/worker"
)

var (
	url string = "https://jsonplaceholder.typicode.com/comments"
	dsn string = "postgres://bobrvmdn:JctYIGUGshfeDFjVHODgfXVPVL2PwZge@cornelius.db.elephantsql.com/bobrvmdn"
)

func main() {
	var wg sync.WaitGroup

	db, err := postgres.New(dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	e := echo.New()
	h := handlers.New(db)
	e.GET("post/:id/comments/statistics", h.HandleStatistics)

	w := worker.New(db, url, 5*time.Minute)

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	wg.Add(1)
	go func() {
		w.Start(ctx)
		wg.Done()
	}()

	go func() {
		if err := e.Start(":8088"); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	<-ctx.Done()
	if err := e.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
	wg.Wait()
}
